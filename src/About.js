import React, {useState} from 'react'

const About = () => {
  const [ data ] = useState({
    nama: "Ikhsanul Akbar Rasyid",
    email: "ikhsanulakbar55@gmail.com",
    os: "Ubuntu",
    gitlab: "gitlab.com/mujaheedsun",
    telegram: "mujaheedsun"
  })

  return (
    <div style={{padding: "10px", border: "2px solid black"}}>
      <h1 style={{textAlign:"center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <ol>
        <li><strong>Nama:</strong> {data.nama}</li> 
        <li><strong>Email:</strong> {data.email} </li> 
        <li><strong>Sistem Operasi yang digunakan:</strong> {data.os}</li>
        <li><strong>Akun Gitlab:</strong> {data.gitlab}</li> 
        <li><strong>Akun Telegram:</strong> {data.telegram}</li> 
      </ol>
    </div>
  )
}

export default About