import React, {useContext, useState, useEffect} from "react"
import axios from "axios"
import {MovieContext} from "./MovieContext"

const MovieForm = () => {

  const [movies, setMovies] = useContext(MovieContext)
  const [input, setInput] = useState({
    title: "",
    description: "",
    year: 0,
    duration: 0,
    genre: "",
    rating: 0,
  })

  useEffect(() => {
    if (movies.status === "startedit") {
      console.log(movies.list)
      console.log(movies.selectedId)
      let dataMovie = movies.list.find(x => x.id === movies.selectedId)
      setInput({
          title: dataMovie.title,
          description: dataMovie.description,
          year: dataMovie.year,
          duration: dataMovie.duration,
          genre: dataMovie.genre,
          rating: dataMovie.rating
      })
      setMovies({...movies, status: "editing"})
    }
  })

  const handleChange = event => {
    let type = event.target.name

    console.log(type)

    switch (type) {
      case "title" : setInput({...input, title: event.target.value})
      case "description" : setInput({...input, description: event.target.value})
      case "year" : setInput({...input, year: event.target.value})
      case "duration" : setInput({...input, duration: event.target.value})
      case "genre" : setInput({...input, genre: event.target.value})
      case "rating" : setInput({...input, rating: event.target.value})
      default:
        break;
    }
  }

  const handleSubmit = event => {
    event.preventDefault();

    if (movies.status === "create") {
      axios.post(`http://backendexample.sanbercloud.com/api/movies`, {
        title: input.title,
        description: input.description,
        year: input.year,
        duration: input.duration,
        genre: input.genre,
        rating: input.rating
      })
      .then( res => {
        setMovies( {
          selectedId: 0, status: "create", list: [...movies.list,
          {id : res.data.id,
            title: input.title,
            description: input.description,
            year: input.year,
            duration: input.duration,
            genre: input.genre,
            rating: input.rating
          }]
        }
        )
      })
    }

    else if(movies.status === "edit") {
      axios.put(`http://backendexample.sanbercloud.com/api/movies/${movies.selectedId}`, {
        title: input.title,
        description: input.description,
        year: input.year,
        duration: input.duration,
        genre: input.genre,
        rating: input.rating,
      })
      .then( res => {
        let dataMovie = movies.list.find( x => x.id === movies.selectedId)
        dataMovie.title = input.title
        dataMovie.description = input.description
        dataMovie.year = input.year
        dataMovie.duration = input.duration
        dataMovie.genre = input.genre
        dataMovie.rating = input.rating
        setMovies({selectedId: 0, status: "create", list: [...movies.list]})
      })
    }

    setInput({
      title: "",
      description: "",
      year: 0,
      duration: 0,
      genre: "",
      rating: 0
    })
  }

  return (
    <>
      <h1>Form Daftar Harga Buah</h1>

      <div style={{width: "50%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
          <form onSubmit={handleSubmit}>

            <label style={{float: "left"}}>
              Title :
            </label>
            <input style={{float: "right"}} type="text" name="title" value={input.title} onChange={handleChange}/>
            <br/>
            <br/>

            <label style={{float: "left"}}>
              Description :
            </label>
            <input style={{float: "right"}} type="text" name="description" value={input.description} onChange={handleChange}/>
            <br/>
            <br/>

            <label style={{float: "left"}}>
              Year :
            </label>
            <input style={{float: "right"}} type="number" name="year" value={input.year} onChange={handleChange}/>
            <br/>
            <br/>

            <label style={{float: "left"}}>
              Duration :
            </label>
            <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
            <br/>
            <br/>

            <label style={{float: "left"}}>
              Genre :
            </label>
            <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
            <br/>
            <br/>

            <label style={{float: "left"}}>
              Rating :
            </label>
            <input style={{float: "right"}} type="number" name="rating" value={input.rating} onChange={handleChange}/>
            <br/>
            <br/>

            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}

export default MovieForm