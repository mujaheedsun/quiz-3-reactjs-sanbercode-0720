import React from "react";
import { Switch, Route } from "react-router-dom";
import Nav from './Nav';
import Home from './Home'
import About from './About'
import MovieListEditor from './MovieListEditor'

// import Login from './Login'

import { UserProvider } from './UserContext';
import { MovieProvider } from './MovieContext'

export default function App() {
  return (
    <>
      <UserProvider>
        <MovieProvider>
          <Nav/>
          <Switch>

            <Route exact path="/">
              <Home />
            </Route>

            <Route path="/about">
              <About />
            </Route>

            <Route path="/movie-list-editor">
              <MovieListEditor />
            </Route>

            {/* <Route path="/login">
              <Login />
            </Route> */}

          </Switch>
        </MovieProvider>
      </UserProvider>
    </>
  );
}