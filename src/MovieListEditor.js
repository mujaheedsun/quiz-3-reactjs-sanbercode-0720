import React, { useContext } from "react"
import { MovieProvider } from "./MovieContext"
import MovieList from "./MovieList"
import MovieForm from "./MovieForm"
import { UserContext } from "./UserContext"

const MovieListEditor = () => {

  const [user] = useContext(UserContext)

  return (
    <>
      <MovieProvider>
        <MovieList/>
        <MovieForm/>
      </MovieProvider>
    </>
  )
}

export default MovieListEditor 