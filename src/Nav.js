import React, { useContext } from "react"
import { Link } from "react-router-dom";
import "./Nav.css"
import { UserContext } from "./UserContext";


const Nav = () =>{
  const [ user ] = useContext(UserContext)

  return(
    <div className='navbar'>
      <img src={'./img/logo.png'} width="200px" alt="Loga tidak ditemukan" />
      <nav>
        <ul>
          <li> <Link to="/"> Home </Link> </li>
          <li> <Link to="/about"> About </Link> </li>
          <li> <Link to="/movie-list-editor"> Movie List Editor </Link> </li>
          <li> <Link to="/login">Login</Link> </li>
        </ul>
      </nav>
    </div>
  )
}

export default Nav
