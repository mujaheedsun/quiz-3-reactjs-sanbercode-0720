import React, {useEffect, useContext} from 'react'
import './Home.css'
import axios from 'axios'
import {MovieContext} from './MovieContext'

const Home = () => {

  const [movies, setMovies] = useContext(MovieContext)

  useEffect( () => {
    if (movies.list === null) {
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        let list = res.data.map(el => {
          return {
            title: el.title,
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating
          }
        })
        list.sort((a,b) => a.rating > b.rating ? -1 : 1)
        console.log(list)
        setMovies({...movies, list})
      })
    }
  })

  return (
    <>
      <section>
        <h1> Daftar Film Film Terbaik</h1>
        {
          
          movies.list !== null && movies.list.map(item => {
            return (
              <div className="movie" key={item.id}>
                <h3> {item.title} <br /> </h3>

                <p> <strong>Rating : {item.rating} <br />
                Durasi : {item.duration} <br />
                Genre : {item.genre} </strong> <br /></p>

                <p> <strong> Deskripsi : </strong> {item.description} </p>
              </div>
            )
          })
        }
      </section>

      <footer>
      <h5>copyright &copy; 2020 by Sanbercode</h5>
    </footer>
    </>
  )
}

export default Home