import React, { useContext, useEffect} from 'react'
import axios from 'axios'
import { MovieContext } from "./MovieContext"
import "./MovieList.css"

const MovieList = () => {

  const [movies, setMovies] = useContext(MovieContext)

  useEffect( () => {
    if (movies.list === null) {
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        console.log(res)
        setMovies({...movies,
          list: res.data.map(el => {
            return {
            title: el.title,
            description: el.description,
            year: el.year,
            duration: el.duration,
            genre: el.genre,
            rating: el.rating,
            id: el.id
            }
          })
        })
      })
    }
  })

  const handleEdit = (event) =>{
    let idMovie = parseInt(event.target.value)
    setMovies({...movies, selectedId: idMovie, status: "startedit"})
  }

  const handleDelete = (event) => {
    let idMovie = parseInt(event.target.value)

    let newMovies = movies.list.filter(el => el.id !== idMovie)

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
    .then(res => {
      console.log(res)
    })
          
    setMovies({...movies, list: [...newMovies]})
    
  }

  return(
    <>
      <h1>Daftar Film Film Terbaik</h1>
      <table>
        <tbody>
        {
          movies.list !== null && movies.list.map((item, index)=>{
            return(
              
              <tr key={item.id}>
                <td>
                <div className="movie">
                  <h3> {item.title} <br /> </h3>
                  <p> <strong>Rating : {item.rating} <br />
                  Durasi : {item.duration} <br />
                  Genre : {item.genre} </strong> <br /></p>
                  <p> <strong> Deskripsi : </strong> {item.description} </p>
                </div>
                </td>
                <td className="button">
                  <button onClick={handleEdit} value={item.id}>Edit</button>
                  <br />
                  <button onClick={handleDelete} value={item.id}>Delete</button>
                </td>
              </tr>
            )
          })
        }
        </tbody>
      </table>      
    </>
  )
}

export default MovieList